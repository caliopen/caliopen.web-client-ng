# Caliopen Web Client

[![Build
Status](https://travis-ci.org/CaliOpen/caliopen.web-client-ng.svg)](https://travis-ci.org/CaliOpen/caliopen.web-client-ng)

[CaliOpen](https://caliopen.org)'s User Interface project.

This project is an Angular application.

## Further Reading / Useful Links
